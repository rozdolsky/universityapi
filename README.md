#University API Documentation
* * *

##/universities

  * **GET** - _List all universities_ 
  * **POST** - _Add a new university to the list_
  * **PUT** - _Update university_
  
###/universities/search

 * **GET** - _Query university by name_ 
  
###/universities{id}

  * **GET** - _Get university by id 
  * **DELETE** - _Delete university id_

##/courses

  * **GET** - _List all courses_ 
  * **POST** - _Add a new course to the list_
  * **PUT** - _Update course if exists_ 

###/courses{id}
  * **GET** - _Get courses by id 
  * **DELETE** - _Delete course by id_

###/courses/search

 * **GET** - _Query course by name_ 
